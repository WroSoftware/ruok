package com.hackyeah.ruok.user.domain;

import com.hackyeah.ruok.session.SessionApi;
import com.hackyeah.ruok.user.model.dto.UserDTO;
import com.hackyeah.ruok.user.model.entity.User;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@Transactional
@Service
@AllArgsConstructor
class UserService {

    private static UserBuilder builder = new UserBuilder();
    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final SessionApi sessionApi;

    public User getUser(Long userId) {
        User user = userRepository.findUserById(userId);
        try {
            return Optional.ofNullable(userId)
                    .map(userRepository::findUserById)
                    .orElseGet(() -> createNewUser(userId));
        } catch (Exception e) {
            return createNewUser(userId);
        }
    }

    private User createNewUser(Long userId) {
        User user = builder.createUser();
        user.setId(userId);
        userRepository.save(user);
        return user;
    }

    public List<UserDTO> getAllUsers() {
        return userRepository.findAll()
                .stream()
                .map(userMapper::mapToDto)
                .map(this::updateWithStatus)
                .collect(toList());
    }

    private UserDTO updateWithStatus(UserDTO userDTO) {
        boolean status = sessionApi.calculateStatus(userDTO.getId());
        userDTO.setStatus(status);
        return userDTO;
    }
}
