package com.hackyeah.ruok.user.model.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class SessionForUserDTO {

    private Long id;
    private LocalDateTime start;
    private LocalDateTime end;
    private int persons;

}
