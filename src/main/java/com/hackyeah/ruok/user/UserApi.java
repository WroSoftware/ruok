package com.hackyeah.ruok.user;

import com.hackyeah.ruok.user.model.dto.SessionForUserDTO;
import com.hackyeah.ruok.user.model.dto.UserDTO;
import com.hackyeah.ruok.user.model.dto.UserSessionDTO;

import java.util.List;

public interface UserApi {

    UserSessionDTO createNewSessionByUserId(Long userId);
    List<UserDTO> getAllUsers();
    List<SessionForUserDTO> getUserSessions(Long userId);

}
