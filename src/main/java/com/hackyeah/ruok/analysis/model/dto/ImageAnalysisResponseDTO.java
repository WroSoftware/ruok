package com.hackyeah.ruok.analysis.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Optional;

@Data
public class ImageAnalysisResponseDTO {

    @JsonProperty("image")
    private String image;
    @JsonProperty("labels")
    private LabelsList labels;

    public String getEmotion() {
        if(labels == null || labels.isEmpty()) {
            return null;
        } else {
           return labels.get(0).getEmotion();
        }
    }
}
