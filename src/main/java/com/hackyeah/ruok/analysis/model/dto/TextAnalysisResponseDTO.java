package com.hackyeah.ruok.analysis.model.dto;

import lombok.Data;

@Data
public class TextAnalysisResponseDTO {

    private String emotion;

}
