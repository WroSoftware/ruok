package com.hackyeah.ruok.analysis.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Labels {

    @JsonProperty("emotion_ex")
    private String emotion;

}
