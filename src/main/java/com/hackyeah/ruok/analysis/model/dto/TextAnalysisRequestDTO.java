package com.hackyeah.ruok.analysis.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Value;

import java.io.Serializable;

@Value
@Builder
public class TextAnalysisRequestDTO implements Serializable {

    @JsonProperty("txt")
    private String text;

}
