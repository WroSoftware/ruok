package com.hackyeah.ruok.analysis.model.dto;

import lombok.Builder;
import lombok.Value;

import java.io.Serializable;

@Value
@Builder
public class ImageAnalysisRequestDTO implements Serializable {

    private String image;
}
