package com.hackyeah.ruok.analysis.domain;


import com.hackyeah.ruok.analysis.model.dto.ImageAnalysisResponseDTO;
import com.hackyeah.ruok.analysis.model.dto.Labels;
import com.hackyeah.ruok.analysis.model.dto.LabelsList;
import com.hackyeah.ruok.analysis.model.dto.TextAnalysisResponseDTO;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Component
class AnalysisApiImpl implements AnalysisApi, ImageEncoder {

    private static final String BEFORE = "base64,";
    private static final List<String> EMOTIONS = Arrays.asList("angry", "scared", "happy", "sad", "neutral");

    @Value("${application.analysis.image}")
    private String imageAnalysisUrl;
    @Value("${application.analysis.text}")
    private String textAnalysisUrl;

    @Override
    public ImageAnalysisResponseDTO analysePicture(String image) {
        return Optional.ofNullable(image)
                .map(this::preProces)
                .map(input -> createRestClient().analyseImage(input))
                .map(this::updateIfNotEmotional)
                .orElse(null);
    }

    @Override
    public TextAnalysisResponseDTO analyseTranscription(String transcription) {
        return createRestClient().analyseText(transcription);
    }

    private AnalysisRestClient createRestClient() {
        return new AnalysisRestClient(imageAnalysisUrl, textAnalysisUrl);
    }
    private String preProces(String input) {
        String substring = input.substring(0, input.indexOf(BEFORE) + BEFORE.length());
        return input.replace(substring, "");
    }


    private ImageAnalysisResponseDTO updateIfNotEmotional(ImageAnalysisResponseDTO responseDTO) {
        boolean present = Optional.ofNullable(responseDTO)
                .map(ImageAnalysisResponseDTO::getEmotion)
                .filter(emotion -> EMOTIONS.contains(emotion))
                .isPresent();
        if(!present){
            Labels labels = new Labels();
            labels.setEmotion("neutral");
            LabelsList list = new LabelsList();
            list.add(labels);
            responseDTO.setLabels(list);
        }
        return responseDTO;
    }

}
