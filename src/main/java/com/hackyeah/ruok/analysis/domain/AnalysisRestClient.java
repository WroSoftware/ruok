package com.hackyeah.ruok.analysis.domain;

import com.hackyeah.ruok.analysis.model.dto.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

class AnalysisRestClient extends RestTemplate {

    private String imageAnalysisUrl;
    private String textAnalysisUrl;

    AnalysisRestClient(String imageAnalysisUrl, String textAnalysisUrl) {
        this.imageAnalysisUrl = imageAnalysisUrl;
        this.textAnalysisUrl = textAnalysisUrl;
    }


    ImageAnalysisResponseDTO analyseImage(String image) {
        try {
            ResponseEntity<ImageAnalysisResponseDTO> response = postForEntity(imageAnalysisUrl, buildImageRequest(image), ImageAnalysisResponseDTO.class);
            return response
                    .getBody();
        } catch (Exception e) {
            e.printStackTrace();
            ImageAnalysisResponseDTO response = new ImageAnalysisResponseDTO();
            response.setImage(image);
            Labels labels = new Labels();
            labels.setEmotion("neutral");
            LabelsList l = new LabelsList();
            l.add(labels);
            response.setLabels(l);
            return response;
        }
    }

    TextAnalysisResponseDTO analyseText(String transcription) {
        return postForEntity(textAnalysisUrl, buildTextRequest(transcription), TextAnalysisResponseDTO.class)
                .getBody();
    }

    private ImageAnalysisRequestDTO buildImageRequest(String image) {
        return ImageAnalysisRequestDTO.builder()
                .image(image)
                .build();
    }

    private TextAnalysisRequestDTO buildTextRequest(String transcription) {
        return TextAnalysisRequestDTO.builder()
                .text(transcription)
                .build();
    }
}
