package com.hackyeah.ruok;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RuokApplication {

	public static void main(String[] args) {
		SpringApplication.run(RuokApplication.class, args);
	}

}
