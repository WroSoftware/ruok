package com.hackyeah.ruok._configuration.spring;


import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@ComponentScan(basePackages="com.hackyeah.ruok")
@EntityScan(basePackages="com.hackyeah.ruok")
@EnableJpaRepositories(basePackages= "com.hackyeah.ruok")
class SpringConfiguration {

}
