package com.hackyeah.ruok.session;

import com.hackyeah.ruok.session.domain.SessionDetailsDTO;
import com.hackyeah.ruok.session.model.dto.ScreenShotWithTranscriptionDTO;
import com.hackyeah.ruok.session.model.dto.SessionDTO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/session")
class SessionController {

    private final SessionApi sessionApi;

    @PostMapping("/{sessionId}/screenshot")
    public void addScreenShotWithTranscription(@PathVariable("sessionId") Long sessionId, @RequestBody ScreenShotWithTranscriptionDTO dto){
        sessionApi.addScreenShotWithTranscription(sessionId, dto);
    }

    @GetMapping("/{sessionId}")
    public SessionDetailsDTO getSessionDetails(@PathVariable("sessionId") Long sessionId) {
        return sessionApi.getSessionDetails(sessionId);
    }
}
