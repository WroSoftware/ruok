package com.hackyeah.ruok.session.model.entity;

import com.hackyeah.ruok.shared.model.entity.BaseEntity;
import lombok.Data;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Data
@Entity
@SequenceGenerator(name = "gen", sequenceName = "image_anal_gen", initialValue = 1, allocationSize = 1)
public class ImageAnalysis extends BaseEntity {


    private String emotion;
    @Type(type="text")
    private String image;

    @OneToOne(fetch = FetchType.LAZY)
    private Session session;

}
