package com.hackyeah.ruok.session;

import com.hackyeah.ruok.session.domain.SessionDetailsDTO;
import com.hackyeah.ruok.session.model.dto.ScreenShotWithTranscriptionDTO;
import com.hackyeah.ruok.session.model.dto.SessionDTO;
import com.hackyeah.ruok.user.model.dto.SessionForUserDTO;
import com.hackyeah.ruok.user.model.entity.User;

import java.util.List;

public interface SessionApi {

    SessionDTO createNewSession(User user);
    void addScreenShotWithTranscription(Long sesionId, ScreenShotWithTranscriptionDTO dto);
    List<SessionForUserDTO> getSessionsForUser(Long userId);
    SessionDetailsDTO getSessionDetails(Long sessionId);
    boolean calculateStatus(Long userId);

}
