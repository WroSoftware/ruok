package com.hackyeah.ruok.session.domain;

import com.hackyeah.ruok.analysis.model.dto.TextAnalysisResponseDTO;
import com.hackyeah.ruok.session.model.entity.Session;
import com.hackyeah.ruok.session.model.entity.TextAnalysis;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
interface TextAnalysisMapper {

    @Mappings({
        @Mapping(target = "id", ignore = true),
        @Mapping(target = "created", ignore = true),
        @Mapping(target = "session", source = "session")
    })
    TextAnalysis mapToEntity(TextAnalysisResponseDTO dto, Session session);
}
