package com.hackyeah.ruok.session.domain;

import com.hackyeah.ruok.session.model.entity.Session;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;

interface SessionRepository extends JpaRepository<Session, Long> {

    List<Session> findAllByUserId(Long userId);

    @Modifying
    @Query("UPDATE Session s SET s.lastUpdate = :now WHERE s.id = :sessionId")
    void updateLastUpdate(LocalDateTime now, Long sessionId);

}
