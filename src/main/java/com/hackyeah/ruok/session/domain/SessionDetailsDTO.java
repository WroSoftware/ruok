package com.hackyeah.ruok.session.domain;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class SessionDetailsDTO {

    private Long id;
    private LocalDateTime start;
    private LocalDateTime end;
    private String transcription;
    private String image;
    private ImageStatisticsDTO statistics;

}
