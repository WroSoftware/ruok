package com.hackyeah.ruok.session.domain;

import com.hackyeah.ruok.session.model.entity.ImageAnalysisStatistic;
import org.springframework.data.jpa.repository.JpaRepository;

interface ImageAnalysisStatisticRepository extends JpaRepository<ImageAnalysisStatistic, Long> {

    ImageAnalysisStatistic findBySessionId(Long id);

}
