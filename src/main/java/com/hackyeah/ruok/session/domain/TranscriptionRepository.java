package com.hackyeah.ruok.session.domain;

import com.hackyeah.ruok.session.model.entity.Transcription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

interface TranscriptionRepository extends JpaRepository<Transcription, Long> {

    @Modifying
    @Query("UPDATE Transcription t SET t.transcription = :transcription WHERE t.session.id = :sessionId")
    void updateTranscription(Long sessionId, String transcription);
    Transcription findBySessionId(Long sessionId);

}
